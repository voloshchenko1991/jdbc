package com.dvo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by User on 11.08.2017.
 */
public class Buyer implements Returnable{
    private int id;
    private String buyerName;
    private String buyerPhone;
    private String buyerAddress;
    private String buyerAuctionNumber;


    public Buyer(ResultSet inputSet) throws SQLException {
        id = inputSet.getInt(1);
        buyerName = inputSet.getString(2);
        buyerPhone = inputSet.getString(3);
        buyerAddress = inputSet.getString(4);
        buyerAuctionNumber = inputSet.getString(5);
    }


    @Override
    public String returnString() {
        return (id + " " + buyerName + " " + buyerPhone + " " + buyerAddress + " " + buyerAuctionNumber);
    }
}
