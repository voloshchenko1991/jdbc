package com.dvo;

/**
 * Created by User on 11.08.2017.
 */
public interface Returnable {
    String returnString();
}
