package com.dvo;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static final String URL = "jdbc:mysql://127.0.0.1:3306/auction";
    public static void main(String[] args) throws SQLException {
        List<Bid> bidList = new ArrayList<>();
        List<Seller> sellerList = new ArrayList<>();
        List<Buyer> buyerList = new ArrayList<>();
        List<Product> productList = new ArrayList<>();

        Connection connection = DriverManager.getConnection(URL,"root", "root");

        Statement statement = connection.createStatement();

        ResultSet resultSetBid = statement.executeQuery("select * from bid");
        while(resultSetBid.next()){
            bidList.add(new Bid(resultSetBid));
        }

        System.out.println("Table Bid:");
        bidList.forEach((a) -> System.out.println(a.returnString()));
        System.out.println("------------------------------------------------\n");


        resultSetBid = statement.executeQuery("select * from product");
        while(resultSetBid.next()){
            productList.add(new Product(resultSetBid));
        }

        System.out.println("Table Product:");
        productList.forEach((a) -> System.out.println(a.returnString()));
        System.out.println("------------------------------------------------\n");


        resultSetBid = statement.executeQuery("select * from buyer");
        while(resultSetBid.next()){
            buyerList.add(new Buyer(resultSetBid));
        }

        System.out.println("Table Buyer:");
        buyerList.forEach((a) -> System.out.println(a.returnString()));
        System.out.println("------------------------------------------------\n");


        resultSetBid = statement.executeQuery("select * from seller");
        while(resultSetBid.next()){
            sellerList.add(new Seller(resultSetBid));
        }

        System.out.println("Table Seller:");
        sellerList.forEach((a) -> System.out.println(a.returnString()));
        System.out.println("------------------------------------------------\n");

        connection.close();
    }
}
