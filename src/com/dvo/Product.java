package com.dvo;


import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by User on 11.08.2017.
 */
public class Product implements Returnable{
    private int id;
    private String productName;
    private int productStartPrice;
    private String productCategory;
    private int sellerId;

    public Product(ResultSet inputSet) throws SQLException {
        id = inputSet.getInt(1);
        productName = inputSet.getString(2);
        productStartPrice = inputSet.getInt(3);
        productCategory = inputSet.getString(4);
        sellerId = inputSet.getInt(5);
    }

    @Override
    public String returnString() {
        return (id + " " + productName + " " + productStartPrice + " " + productCategory + " " + sellerId);
    }
}
