package com.dvo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by User on 11.08.2017.
 */
public class Seller implements Returnable{
    private int id;
    private String sellerName;
    private String sellerAddress;
    private String sellerPhone;
    private String sellerLicenseNumber;



    public Seller(ResultSet inputSet) throws SQLException {
        id = inputSet.getInt(1);
        sellerName = inputSet.getString(2);
        sellerAddress = inputSet.getString(3);
        sellerPhone = inputSet.getString(4);
        sellerLicenseNumber = inputSet.getString(5);
    }

    @Override
    public String returnString() {
        return (id + " " + sellerName + " " + sellerAddress + " " + sellerPhone + " " + sellerLicenseNumber);
    }
}
