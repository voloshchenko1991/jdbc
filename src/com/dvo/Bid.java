package com.dvo;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by User on 11.08.2017.
 */
public class Bid implements Returnable {
    private int id;
    private int bidStep;
    private int currentBid;
    private int buyerId;
    private int productId;

    public Bid(ResultSet inputSet)  throws SQLException {
        id = inputSet.getInt(1);
        bidStep = inputSet.getInt(2);
        currentBid = inputSet.getInt(3);
        buyerId = inputSet.getInt(4);
        productId = inputSet.getInt(5);
    }


    @Override
    public String returnString() {
        return(id + " " + bidStep + " " + currentBid + " " + buyerId + " " + productId);
    }
}
